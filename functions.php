<?php

if (!function_exists('ew_setup')) :
    function ew_setup()
    {
        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');
        add_theme_support('automatic-feed-links');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(
            array(
                'header_menu' => esc_html__('Menu principal', 'ew'),
                'footer_menu' => esc_html__('Menu pied de page', 'ew'),
            )
        );

        // Enqueue scripts and styles. 
        function ew_styles_scripts()
        {

            /*wp_enqueue_script('swiper',  get_template_directory_uri() . '/assets/lib/swiperjs/swiper-bundle.min.js', array(), null, true);
            wp_enqueue_script('swiper',  get_template_directory_uri() . '/assets/lib/swiperjs/swiper-bundle.min.css', array());*/

            /*wp_enqueue_script('aos',  get_template_directory_uri() . '/assets/lib/aos/aos.js', array(), null, true);
            wp_enqueue_style('aos',  get_template_directory_uri() . '/assets/lib/aos/aos.js', array());*/

            wp_enqueue_style('lineawesome',  get_template_directory_uri() . '/assets/font/lineawesome/css/line-awesome.min.css', array());

            wp_enqueue_script('ew',  get_template_directory_uri() . '/src/js/ew.js', array(), null, true);
            wp_enqueue_style('ew', get_template_directory_uri() . '/assets/css/ew.css', array());
        }
        add_action('wp_enqueue_scripts', 'ew_styles_scripts');
    }
endif;
add_action('after_setup_theme', 'ew_setup');

require_once(__DIR__ . '/config/options.php');
require_once(__DIR__ . '/config/tools.php');
