<?php

/**
 * Personnaliser la page de connexion
 */
function changer_logo_login_page()
{
?>
    <style type="text/css">
        body.login {
            background-color: black;
        }

        body.login div#login h1 a {
            background-image: url(<?= get_image_repo('.svg') ?>) !important;
            padding-bottom: 0;
            width: 65%;
            background-size: contain;
            background-position: center;
            height: 110px;
            background-image: none;
            background-repeat: no-repeat;
        }

        body.login #backtoblog a,
        body.login #nav a,
        body.login .privacy-policy-link {
            color: white !important;
        }

        body.login #nav {
            text-align: center;
        }
    </style>
<?php
}
add_action('login_enqueue_scripts', 'changer_logo_login_page');

// Enlever prefixe dans archive title
add_filter('get_the_archive_title', function ($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_tax()) { //for custom post types
        $title = sprintf(__('%1$s'), single_term_title('', false));
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    }
    return $title;
});


// Disable full screen by default 
if (is_admin()) {
    function jba_disable_editor_fullscreen_by_default()
    {
        $script = "jQuery( window ).load(function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } });";
        wp_add_inline_script('wp-blocks', $script);
    }
    add_action('enqueue_block_editor_assets', 'jba_disable_editor_fullscreen_by_default');
}
