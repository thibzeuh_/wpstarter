<?php

// Raccourcir une chaine
function shorten_string($string, $wordsreturned)
{
    $retval = $string;
    $string = preg_replace('/(?<=\S,)(?=\S) /', ' ', $string);
    $string = str_replace("\n", " ", $string);
    $array = explode(" ", $string);
    if (count($array) <= $wordsreturned) {
        $retval = $string;
    } else {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array) . " ...";
    }
    return $retval;
}


// Fonction raccourci pour récuperer l'url d'une image des assets.
function get_image_repo($file)
{
    return get_template_directory_uri() . "/assets/img/" . $file;
}

function print_svg($file)
{
    $iconfile = new DOMDocument();
    $iconfile->load($file);
    echo $iconfile->saveHTML($iconfile->getElementsByTagName('svg')[0]);
}

// Remove tags support from posts
function myprefix_unregister_tags()
{
    unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'myprefix_unregister_tags');

add_theme_support('disable-custom-colors');

add_theme_support(
    'editor-color-palette',
    array(
        array(
            'name'  => esc_html__('Noir'),
            'slug'  => 'noir',
            'color' => '#000000',
        ),
    )
);
