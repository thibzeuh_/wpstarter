module.exports = {
  plugins: [
    require("postcss-import-ext-glob"),
    require("postcss-import"),
    require("postcss-bem"),
    require("postcss-nested"),
    require("postcss-mixins"),
    require("autoprefixer"),
    require("cssnano"),
  ],
};
